#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "user_list.h"
#include "contas_list.h"
//#include "servicos_list.h" // TODO: para comentar e meter no ficheiro principal

struct contalist* contalist_new()
{
	struct contalist* newlist=malloc(sizeof(struct contalist));
	newlist->head=NULL;
	return newlist;
}


struct conta_node* conta_next(struct conta_node* current)
{
	return current->next;
}


bool contalist_empty(struct contalist* list)
{
	return list->head==NULL;
}


struct conta_node * contalist_insert_node(struct conta_node* head, struct userlist* userlist, char* usr, char nib[])
{
	struct conta_node* new_node = malloc(sizeof(struct conta_node));
	//struct movimentoslist* movimentos=movimentoslist_new();
	strcpy(new_node->movimentos, "Conta_criada.");
	strcpy(new_node->user,usr);
	strcpy(new_node->nib,nib);
	new_node->user_node=get_user(userlist, usr);
	new_node->saldo=1000;
	new_node->congelada=false;
	new_node->next=head->next;
	head->next=new_node;
	return new_node;
}


struct conta_node * contalist_insert(struct contalist *list, struct userlist* userlist, char* usr, char nib[])
{
	if(contalist_empty(list)){
		list->head=malloc(sizeof(struct conta_node));
		//struct movimentoslist* movimentos=movimentoslist_new();
		strcpy(list->head->movimentos, "Conta_criada.");
		strcpy(list->head->user,usr);
		strcpy(list->head->nib,nib);
		list->head->user_node=get_user(userlist, usr);
		list->head->saldo=1000;
		list->head->congelada=false;
		return list->head;
	}
	else
		return contalist_insert_node(list->head, userlist, usr, nib);
}


void contalist_print(struct contalist *list)
{
	struct conta_node* current=list->head;
	while(current!=NULL){
		printf("%s (%s) ", current->nib, current->user);
		current=conta_next(current);
		if(current!=NULL)
			printf("| ");
	}
	printf("\n");
}


int contalist_length(struct contalist *list)
{
	if(contalist_empty(list))
		return 0;
	else{
		int counter=0;
		struct conta_node* current=list->head;
		while(current!=NULL){
			counter++;
			current=conta_next(current);
		}
		return counter;
	}
}

void contalist_destroy(struct contalist *list)
{
	if(!contalist_empty(list)){
		struct conta_node* current=list->head;
		while(current!=NULL){
			struct conta_node* temp;
			temp=conta_next(current);
			free(current);
			current=temp;
		}
		free(list);
	}
}


void contanode_destroy(struct contalist* list, char* nib)
{
	struct conta_node* current=list->head;
	struct conta_node* temp;
	struct conta_node* ant=list->head;
	if(strcmp(current->nib, nib)==0){
		temp = list->head;
		list->head = list->head->next;
		free(temp);
	}
	else{
		while(current!=NULL){
			if(strcmp(current->nib, nib)==0){
				if(conta_next(current)!=NULL){
					temp=conta_next(current);
					ant->next = temp;
				}
				else
					ant->next = NULL;
				free(current);
				break;
			}
			ant = current;
			current=conta_next(current);
		}
	}
}

char* conta_remove(struct contalist* list, struct userlist* userlist, char* user, char* nib1, char* nib2)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct conta_node* current=list->head;
		struct conta_node* conta1=NULL;
		struct conta_node* conta2=NULL;
		while(current!=NULL){
			if(conta1 == NULL && strcmp(nib1, current->nib)==0)
				conta1 = current;
			if(conta2 == NULL && strcmp(nib2, current->nib)==0)
				conta2 = current;
			if(current->next == NULL)
				break;
			current=conta_next(current);
		}
		if(conta1 == NULL || conta2 == NULL){
			return "2110"; // Erro na operação
		}
		else{
			if(strcmp(user, conta1->user)==0 && strcmp(user, conta2->user)==0){
				if(conta1->saldo >= 0 && !conta1->congelada){
					conta2->saldo += conta1->saldo;
					contanode_destroy(list, nib1);
					return "2109"; // Operação efetuada com Sucesso
				}
				else
					return "2110"; // Erro na operação
			}
			else
				return "2110"; // Erro na operação
		}
	}
}

char* cria_conta(struct contalist* list, struct userlist* userlist, char* user)
{
	struct user_node* user_node=get_user(userlist, user);
	printf("nivel %d\n", user_node->nivel);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes

    int n = (rand() % 9999999999) + 10000000000;
    char nib[21] = "3", nib2[10];
    sprintf(nib2, "%d", abs(n));
    strcat(nib, nib2);
    n = (rand() % 99999999) + 10000000000;
    sprintf(nib2, "%d", abs(n));
    strcat(nib, nib2);

    if(strlen(nib)==20){
    	n = rand() % 10;
    	sprintf(nib2, "%d", abs(n));
    	strcat(nib, nib2);
    }

    //printf("nib: %s\n", nib);
	contalist_insert(list, userlist, user, nib);
	return "2100"; // Conta feita com sucesso
}

char* lista_contas(struct contalist* list, struct userlist* userlist, char* user)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct conta_node* current=list->head;
		char * contas = malloc(sizeof (char) * 1000);
		sprintf(contas, "2102 ");
		bool tem_contas = false;
		while(current!=NULL){
			if(strcmp(user, current->user)==0){
				tem_contas = true;
				strcat(contas, current->nib);
				strcat(contas, ".");
			}
			current=conta_next(current);
		}
		if(tem_contas){
			strcat(contas, "\0");
			return contas; // O seu NIB é X
		}
		return "2110"; // Erro na operação
	}
}

char* consulta_saldo(struct contalist* list, struct userlist* userlist, char* user, char* nib)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct conta_node* current=list->head;
		while(current!=NULL){
			if(strcmp(nib, current->nib)==0 && strcmp(user, current->user)==0){
				char * saldo = malloc(sizeof (char) * 100);
			    sprintf(saldo, "2103 %d", current->saldo);
				return saldo; // O seu saldo é X
			}
			current=conta_next(current);
		}
		return "2110"; // Erro na operação
	}
}

char* consulta_saldo_integrado(struct contalist* list, struct userlist* userlist, char* user)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct conta_node* current=list->head;
		char * saldo_ch = malloc(sizeof (char) * 100);
		int saldo = 0;
		bool tem_contas = false;
		while(current!=NULL){
			if(strcmp(user, current->user)==0){
				tem_contas = true;
				saldo += current->saldo;
			}
			current=conta_next(current);
		}
		if(tem_contas){
			sprintf(saldo_ch, "2106 %d", saldo);
			return saldo_ch; // O saldo total das suas contas é X
		}
		return "2110"; // Erro na operação
	}
}

char* consulta_movimentos(struct contalist* list, struct userlist* userlist, char* user, char* nib)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct conta_node* current=list->head;
		char * movimentos = malloc(sizeof (char) * 5000);
		while(current!=NULL){
			if(strcmp(nib, current->nib)==0 && strcmp(user, current->user)==0){
				sprintf(movimentos, "2105 %s", current->movimentos);
				return movimentos;
			}
			current=conta_next(current);
		}
		return "2110"; // Erro na operação
	}
}

char* transferencia(struct contalist* list, struct userlist* userlist, char* user, char* nib1, char* nib2, int valor)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 1 && user_node->nivel != 2)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct conta_node* current=list->head;
		struct conta_node* conta1=NULL;
		struct conta_node* conta2=NULL;
		while(current!=NULL){
			if(conta1 == NULL && strcmp(nib1, current->nib)==0)
				conta1 = current;
			if(conta2 == NULL && strcmp(nib2, current->nib)==0)
				conta2 = current;
			if(current->next == NULL)
				break;
			current=conta_next(current);
		}
		if(conta1 == NULL || conta2 == NULL)
			return "2110"; // Erro na operação
		else{
			if(strcmp(user, conta1->user)==0 && strcmp(user, conta2->user)==0){
				if(conta1->congelada)
					return "2110"; // Erro na operação
				if(conta1->saldo < valor)
					return "2107"; // Conta sem saldo, impossível efetuar transferência
				else{
					conta1->saldo -= valor;
					conta2->saldo += valor;
					char tr[100] = "";
					sprintf(tr, "Transferência_de_%d_euros_efetuada.", valor);
					strcat(conta1->movimentos, tr);
					sprintf(tr, "Transferência_de_%d_euros_recebida.", valor);
					strcat(conta2->movimentos, tr);
					return "2109"; // Operação efetuada com Sucesso
				}
			}
			else
				return "2110"; // Erro na operação
		}
	}
}

// char* pagamento(struct contalist* contalist, struct servicoslist* servicoslist, char* user, char* nib1, char* servico);

char* congela_conta(struct contalist* list, struct userlist* userlist, char* user, char* nib)
{
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct user_node* user_node=get_user(userlist, user);
		if(user_node==NULL){
			return "2110"; // Erro na operação
		}
		else{
			if(user_node->nivel != 3)
				return "4108"; // Erro: Nao tem permissoes
			else{
				struct conta_node* current=list->head;
				while(current!=NULL){
					if(strcmp(nib, current->nib)==0){
						if(current->congelada)
							return "4107"; // Erro: Conta já congelada
						else{
							current->congelada = true;
							strcat(current->movimentos, "Conta_congelada.");
							return "4104"; // Conta congelada com sucesso
						}
					}
					current=conta_next(current);
				}
				return "2110"; // Erro na operação
			}
		}
	}
}


char* descongela_conta(struct contalist* list, struct userlist* userlist, char* user, char* nib)
{
	if(contalist_empty(list))
		return "2110"; // Erro na operação
	else{
		struct user_node* user_node=get_user(userlist, user);
		if(user_node==NULL){
			return "2110"; // Erro na operação
		}
		else{
			if(user_node->nivel != 3)
				return "4108"; // Erro: Nao tem permissoes
			else{
				struct conta_node* current=list->head;
				while(current!=NULL){
					if(strcmp(nib, current->nib)==0){
						if(!current->congelada)
							return "4106"; // Erro: Conta não congelada
						else{
							current->congelada = false;
							strcat(current->movimentos, "Conta_descongelada.");
							return "4105"; // Conta descongelada com sucesso
						}
					}
					current=conta_next(current);
				}
				return "2110"; // Erro na operação
			}
		}
	}
}


char* total_depositado(struct contalist* list, struct userlist* userlist, char* user)
{
	struct user_node* user_node=get_user(userlist, user);
	if(user_node->nivel != 3)
		return "4108"; // Erro: Nao tem permissoes
	if(contalist_empty(list))
		return "4103 0"; // Valor total de dinheiro
	else{
		struct conta_node* current=list->head;
		char * depositado = malloc(sizeof (char) * 100);
		int total = 0;
		while(current!=NULL){
			total += current->saldo;
			current=conta_next(current);
		}
		sprintf(depositado, "4103 %d", total);
		return depositado; // Valor total de dinheiro
	}
}

/*
int main()
{
	srand(time(NULL)); // chamar só uma vez
	struct userlist* userlist = userlist_new();
	struct contalist* contalist = contalist_new();
	printf("%s\n", user_register(userlist, "joao", "qwerty", 3));

	char cod[5], received[1000], arg1[100], arg2[100];

	//printf("%s\n", lista_contas(contalist, "pedro"));
	printf("%s\n", cria_conta(contalist, userlist, "joao"));
	printf("%s\n", cria_conta(contalist, userlist, "joao"));
	printf("%s\n", cria_conta(contalist, userlist, "joana"));
	//printf("%s\n", lista_contas(contalist, "pedro"));

	strcpy(received, lista_contas(contalist, "joao"));
	sscanf(received, "%s %s\n%s", cod, arg1, arg2);
	printf("cod: %s\narg1: %s\narg2: %s\n", cod, arg1, arg2);

	//contalist_print(contalist);
	//printf("%s: %s\n", arg1, consulta_saldo(contalist, "joao", arg1));
	//printf("%s\n", consulta_saldo_integrado(contalist, "joao"));
	//printf("%s\n", consulta_saldo_integrado(contalist, "joana"));
	//printf("%s\n", total_depositado(contalist, userlist, "joao"));

	printf("%s\n", transferencia(contalist, "joao", arg1, arg2, 500));
	//printf("%s: %s\n", arg1, consulta_saldo(contalist, "joao", arg1));
	//printf("%s: %s\n", arg2, consulta_saldo(contalist, "joao", arg2));

	printf("%s\n", congela_conta(contalist, userlist, "joao", arg1));
	printf("%s\n", descongela_conta(contalist, userlist, "joao", arg1));
	//printf("%s\n", conta_remove(contalist, "joao", arg2, arg1));
	printf("%s\n", consulta_movimentos(contalist, "joao", arg1));

	return 0;
}
*/