#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#ifdef WIN32	// se o SO for windows
#include <winsock2.h>
WSADATA wsa_data;
#else			// se o SO for linux/unix
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

int main()
{
	#ifdef WIN32	// se o SO for windows
	WSAStartup(MAKEWORD(2, 0), &wsa_data);
	#endif

	//criar o socket	--------------------------------------------------------
	int sock;
	
	// Definição e população da estructura sockaddr_in -------------------------
	struct sockaddr_in server;
	memset(&(server.sin_zero),0, sizeof(server.sin_zero));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr("127.0.0.1");	//define o IP
	server.sin_port = htons(2000);						//define a porta [1012-65535]

	char to_send[2048];	// variável para guardar o que é escrito do socket
	char received[2048]; // variável para guardar a resposta do servidor
	char command[4], args1[32], args2[32], args3[128], arg_received[2048], cod_receive[3];

	bool is_sending = true;

	char cod[1024]; // código e mensagem ser enviada

	while(1){
		sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

		fgets(to_send, 300, stdin);

		int n_args = sscanf(to_send, "%s %s %s %s", command, args1, args2, args3);

		if(n_args == 1){
			if(strcmp("cl", command)==0){ // lista contas
				sprintf(cod, "%s", "2006");
			}
			else if(strcmp("cc", command)==0){ // cria conta
				sprintf(cod, "%s", "2000");
			}
			else if(strcmp("csi", command)==0){ // consulta saldo integrado
				sprintf(cod, "%s", "2003");
			}
			else if(strcmp("alc", command)==0){ // lista todos os clientes e saldos
				sprintf(cod, "%s", "4001");
			}
			else if(strcmp("ale", command)==0){ // lista todas as entidades e saldos
				sprintf(cod, "%s", "4002");
			}
			else if(strcmp("alv", command)==0){ // lista o total depositado no banco
				sprintf(cod, "%s", "4003");
			}
			else {
				cod[0] = '\0';
				printf("\n");
				int print_cmd;
				FILE * command_list = fopen("utilizacao.txt", "r");
				if(command_list){
				    while((print_cmd = getc(command_list)) != EOF)
				        putchar(print_cmd);
				    fclose(command_list);
				}
			}
		}
		else if(n_args == 2){
			if(strcmp("cs", command)==0){ // consulta saldo
				sprintf(cod, "%s %s", "2001", args1);
			}
			else if(strcmp("cm", command)==0){ // consulta movimentos da conta
				sprintf(cod, "%s %s", "2002", args1);
			}
			else if(strcmp("acc", command)==0){ // congela conta
				sprintf(cod, "%s %s", "4004", args1);
			}
			else if(strcmp("adc", command)==0){ // congela conta
				sprintf(cod, "%s %s", "4005", args1);
			}
			else{
				is_sending = false;
				printf("Comando errado.\n\n");
			}
		}
		else if(n_args == 3){
			if(strcmp("l", command)==0){
				sprintf(cod, "%s %s %s", "1000", args1, args2); // login
			}
			else if(strcmp("rc", command)==0){
				sprintf(cod, "%s %s %s", "1001", args1, args2); // regista cliente
			}
			else if(strcmp("re", command)==0){
				sprintf(cod, "%s %s %s", "1002", args1, args2); // regista entidade
			}
			else if(strcmp("ra", command)==0){
				sprintf(cod, "%s %s %s", "1003", args1, args2); // regista admin
			}
			else if(strcmp("cr", command)==0){
				sprintf(cod, "%s %s %s", "2005", args1, args2); // apaga [conta1] e transfere saldo para [conta2]
			}
			else if(strcmp("sc", command)==0){
				sprintf(cod, "%s %s %s", "3001", args1, args2); // cria servico de pagamento com o [nome_servico] 
			}
			else if(strcmp("sp", command)==0){
				sprintf(cod, "%s %s %s", "3002", args1, args2); // efetua pagamento do [nome_servico] da conta [nib]
			}
			else{
				is_sending = false;
				printf("Comando errado.\n");
			}
		}
		else if(n_args == 4){
			if(strcmp("ct", command)==0){
				sprintf(cod, "%s %s %s %s", "2004", args1, args2, args3); // transfere valor da conta1 para a conta2
			}
			else{
				is_sending = false;
				printf("Comando errado.\n");
			}
		}
		if(is_sending == true){
			int request = connect(sock, (struct sockaddr *)&server,sizeof(server));

			if(request == -1)
				printf("Conexão não encontrada.\n");
			else{
				send(sock, cod, strlen(to_send)+1, 0);
				recv(sock, received, sizeof(received)+1, 0);
				n_args = sscanf(received, "%s %s", cod_receive, arg_received);

				if(n_args == 1){
					if(cod_receive[0] == '1' && cod_receive[1] == '1'){ // 11XX
						if(cod_receive[2] == '0'){ // 110X
							if(cod_receive[3] == '0'){ // 1100 - Utilizador identificado com sucesso
								printf("Utilizador identificado com sucesso.\n");
							}
							else if(cod_receive[3] == '1'){ // 1101 - Utilizador registado com sucesso
								printf("Utilizador registado com sucesso.\n");
							}
							else if(cod_receive[3] == '3'){ // 1103 - Username já existente no sistema, insira novo username
								printf("Username já existente no sistema, insira novo username.\n");
							}
							else if(cod_receive[3] == '4'){ // 1104 - Login incorreto, tente novamente
								printf("Login incorreto, tente novamente.\n");
							}
							else if(cod_receive[3] == '5'){ // 1105 - Registo incorreto
								printf("Registo incorreto.\n");
							}
						}
					}
					else if(cod_receive[0] == '2' && cod_receive[1] == '1'){ // 21XX
						if(cod_receive[2] == '0'){ // 210X
							if(cod_receive[3] == '0'){ // 2100 - Conta feita com sucesso
								printf("Conta feita com sucesso.\n");
							}
							else if(cod_receive[3] == '1'){ // 2101 - Insucesso na criação de conta
								printf("Insucesso na criação de conta.\n");
							}
							else if(cod_receive[3] == '7'){ // 2107 - Conta sem saldo, impossível efetuar transferência
								printf("Conta sem saldo, impossível efetuar transferência.\n");
							}
							else if(cod_receive[3] == '9'){ // 2109 - Operação efetuada com Sucesso
								printf("Operação efetuada com sucesso.\n");
							}
						}
						else if(cod_receive[2] == '1'){ // 211X
							if(cod_receive[3] == '0'){ // 2110 - Erro na operação
								printf("Erro na operação.\n");
							}
							else if(cod_receive[3] == '1'){ // 2111 - Nao tem movimentos
								printf("Nao tem movimentos.\n");
							}
						}
					}
					else if(cod_receive[0] == '3' && cod_receive[1] == '1'){ // 31XX
						if(cod_receive[2] == '0'){ // 310X
							if(cod_receive[3] == '1'){ // 3101 - Serviço feito
								printf("Serviço feito.\n");
							}
							else if(cod_receive[3] == '2'){ // 3102 - Não foi permitido a criação do serviço
								printf("Não foi permitido a criação do serviço.\n");
							}
							else if(cod_receive[3] == '3'){ // 3103 - Pagamento efetuado
								printf("Pagamento efetuado.\n");
							}
							else if(cod_receive[3] == '4'){ // 3104 - Pagamento não efetuado por falta de saldo
								printf("Pagamento não efetuado por falta de saldo.\n");
							}
							else if(cod_receive[3] == '5'){ // 3105 - Serviço não disponível
								printf("Serviço não disponível.\n");
							}
							else if(cod_receive[3] == '6'){ // 3106 - Nome do Serviço já existente no sistema
								printf("Nome do Serviço já existente no sistema.\n");
							}
						}
					}
					else if(cod_receive[0] == '4' && cod_receive[1] == '1'){ // 41XX
						if(cod_receive[2] == '0'){ // 410X
							if(cod_receive[3] == '4'){ // 4104 - Conta congelada com sucesso
								printf("Conta congelada com sucesso.\n");
							}
							else if(cod_receive[3] == '5'){ // 4105 - Conta descongelada com sucesso
								printf("Conta descongelada com sucesso.\n");
							}
							else if(cod_receive[3] == '6'){ // 4106 - Erro: Conta não congelada
								printf("Erro: Conta não congelada.\n");
							}
							else if(cod_receive[3] == '7'){ // 4107 - Erro: Conta já congelada
								printf("Erro: Conta já congelada.\n");
							}
							else if(cod_receive[3] == '8'){ // 4108 - Erro: Nao tem permissoes
								printf("Erro: Nao tem permissoes.\n");
							}
						}
					}
					else if(cod_receive[0] == '5' && cod_receive[1] == '0'){ // 50XX
						if(cod_receive[2] == '0'){ // 500X
							if(cod_receive[3] == '0'){ // 5000 - Precisa de login para efetuar essa operação
								printf("Precisa de login para efetuar essa operação.\n");
							}
							else if(cod_receive[3] == '1'){ // 5001 - Comando inexistente
								printf("Comando inexistente.\n");
							}
						}
					}
				}
				else if(n_args == 2){
					if(cod_receive[0] == '2' && cod_receive[1] == '1'){ // 21XX
						if(cod_receive[2] == '0'){ // 210X
							if(cod_receive[3] == '2'){ // 2102 - O seu NIB é X
								printf("Listagem de contas:\n");
								char* pch;
								pch = strtok(arg_received, ".");
								int i = 1;
								while(pch != NULL){
									printf ("%d: %s\n", i, pch);
									pch = strtok(NULL, ".");
									i += 1;
								}
							}
							else if(cod_receive[3] == '3'){ // 2103 - O seu saldo é X
								printf("Tem um saldo de %s euros.\n", arg_received);
							}
							else if(cod_receive[3] == '5'){ // 2105 - Histórico de movimento da conta X
								printf("Historico de movimentos:\n");
								char* pch;
								pch = strtok(arg_received, ".");
								int i = 1;
								while(pch != NULL){
									printf ("%d: %s\n", i, pch);
									pch = strtok(NULL, ".");
									i += 1;
								}
							}
							else if(cod_receive[3] == '6'){ // 2106 - O saldo total das suas contas é X
								printf("O saldo total das suas contas é de %s euros.\n", arg_received);
							}
						}
					}
					else if(cod_receive[0] == '3' && cod_receive[1] == '1'){ // 31XX
						if(cod_receive[2] == '0'){ // 310X
							if(cod_receive[3] == '0'){ // 3100 - Lista de serviços
								printf("Lista de serviços:\n");
								char* pch;
								pch = strtok(arg_received, ".");
								int i = 1;
								while(pch != NULL){
									printf ("%d: %s\n", i, pch);
									pch = strtok(NULL, ".");
									i += 1;
								}
							}
						}
					}
					else if(cod_receive[0] == '4' && cod_receive[1] == '1'){ // 41XX
						if(cod_receive[2] == '0'){ // 410X
							if(cod_receive[3] == '1'){ // 4101 - Lista com os clientes e suas contas
								printf("Lista de clientes e contas:\n");
								char* pch;
								pch = strtok(arg_received, ".");
								int i = 1;
								while(pch != NULL){
									printf ("%d: %s\n", i, pch);
									pch = strtok(NULL, ".");
									i += 1;
								}
							}
							else if(cod_receive[3] == '2'){ // 4102 - Lista com os Entidades e suas contas
								printf("Lista de Entidades e contas:\n");
								char* pch;
								pch = strtok(arg_received, ".");
								int i = 1;
								while(pch != NULL){
									printf ("%d: %s\n", i, pch);
									pch = strtok(NULL, ".");
									i += 1;
								}
							}
							else if(cod_receive[3] == '3'){ // 4103 - Valor total de dinheiro
								printf("Valor total de dinheiro depositado: %s euros.\n", arg_received);
							}
						}
					}
				}
			}
		send(sock, cod, strlen(cod)+1, 0);
		printf("\n");
		}

	//is_sending = true;
	close(sock);
	}

}