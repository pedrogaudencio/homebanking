#ifndef userlist_h
#define userlist_h

/* userlist interface */

struct user_node{
	struct user_node* next;
	char user[15];
	char password[15];
	int nivel; // 1 - cliente, 2 - entidade, 3 - admin
};

struct userlist{
	struct user_node* head;
};

struct userlist* userlist_new();

struct user_node* user_next(struct user_node* current);

bool userlist_empty(struct userlist* list);

void userlist_destroy(struct userlist* list);

struct user_node * userlist_insert_node(struct user_node* head, char usr[], char pw[], int n);
struct user_node * userlist_insert(struct userlist *list, char usr[], char pw[], int n);

void userlist_print(struct userlist *list);

int userlist_length(struct userlist *list);

void userlist_destroy(struct userlist* list);

int userlist_remove(struct userlist *lst, char* user);

void userlist_print(struct userlist *userlist);

struct user_node* get_user(struct userlist *list, char* user);

char* user_login(struct userlist* userlist, char* user, char* pass);
char* user_register(struct userlist* userlist, char* user, char* pass, int nivel);

#endif /* userlist_h */