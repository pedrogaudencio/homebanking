#ifndef contaslist_h
#define contaslist_h

/* contas interface */

struct conta_node{
	struct conta_node* next;
	char user[15];
	struct user_node* user_node;
	char nib[25];
	int saldo;
	bool congelada;
	char movimentos[5000];
};

struct contalist{
	struct conta_node* head;
};

struct contalist* contalist_new();

struct conta_node* conta_next(struct conta_node* current);

bool contalist_empty(struct contalist* list);

void contalist_destroy(struct contalist* list);

struct conta_node * contalist_insert_node(struct conta_node* head, struct userlist* userlist, char* usr, char nib[]);
struct conta_node * contalist_insert(struct contalist *list, struct userlist* userlist, char* user, char nib[]);

void contalist_print(struct contalist *list);

int contalist_length(struct contalist *list);

void contalist_destroy(struct contalist* list);

int contalist_remove(struct contalist *list);

void contalist_print(struct contalist *list);

void contanode_destroy(struct contalist* list, char* nib);
char* conta_remove(struct contalist* list, struct userlist* userlist, char* user, char* nib1, char* nib2);

char* cria_conta(struct contalist* list, struct userlist* userlist, char* user);
char* lista_contas(struct contalist* list, struct userlist* userlist, char* user);

char* consulta_saldo(struct contalist* list, struct userlist* userlist, char* user, char* nib);
char* consulta_saldo_integrado(struct contalist* list, struct userlist* userlist, char* user);

char* consulta_movimentos(struct contalist* list, struct userlist* userlist, char* user, char* nib);

char* transferencia(struct contalist* list, struct userlist* userlist, char* user, char* nib1, char* nib2, int valor);

// char* pagamento(struct contalist* contalist, struct servicoslist* servicoslist, char* user, char* nib1, char* servico);

char* congela_conta(struct contalist* list, struct userlist* userlist, char* user, char* nib);
char* descongela_conta(struct contalist* list, struct userlist* userlist, char* user, char* nib);

char* total_depositado(struct contalist* list, struct userlist* userlist, char* user);

#endif /* contalist_h */