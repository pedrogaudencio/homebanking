#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "user_list.h"


struct userlist* userlist_new()
{
	struct userlist* newlist=malloc(sizeof(struct userlist));
	newlist->head=NULL;
	return newlist;
}


struct user_node* user_next(struct user_node* current)
{
	return current->next;
}


bool userlist_empty(struct userlist* list)
{
	return list->head==NULL;
}


struct user_node * userlist_insert_node(struct user_node* head, char usr[], char pw[], int n)
{
	struct user_node* new_node = malloc(sizeof(struct user_node));
	strcpy(new_node->user,usr);
	strcpy(new_node->password,pw);
	new_node->nivel=n;
	new_node->next=head->next;
	head->next=new_node;
	return new_node;
}


struct user_node * userlist_insert(struct userlist *list, char usr[], char pw[], int n)
{
	if(userlist_empty(list)){
		list->head=malloc(sizeof(struct user_node));
		strcpy(list->head->user,usr);
		strcpy(list->head->password,pw);
		list->head->nivel=n;
		return list->head;
	}
	else
		return userlist_insert_node(list->head, usr, pw, n);
}


void userlist_print(struct userlist *list)
{
	struct user_node* current=list->head;
	while(current!=NULL){
		printf("%s (%d) ", current->user, current->nivel);
		current=user_next(current);
		if(current!=NULL)
			printf("| ");
	}
	printf("\n");
}


int userlist_length(struct userlist *list)
{
	if(userlist_empty(list))
		return 0;
	else{
		int counter=0;
		struct user_node* current=list->head;
		while(current!=NULL){
			counter++;
			current=user_next(current);
		}
		return counter;
	}
}

void userlist_destroy(struct userlist *list)
{
	if(!userlist_empty(list)){
		struct user_node* current=list->head;
		while(current!=NULL){
			struct user_node* temp;
			temp=user_next(current);
			free(current);
			current=temp;
		}
		free(list);
	}
}


struct user_node* get_user(struct userlist *userlist, char* user)
{
	struct user_node* current_usr=userlist->head;
	if(!userlist_empty(userlist)){
		while(current_usr!=NULL){
			if(strcmp(user, current_usr->user)==0)
				return current_usr;
			current_usr=user_next(current_usr);
		}
	}
	return NULL;
}


char* user_login(struct userlist* list, char* user, char* pass)
{
	struct user_node* current=list->head;
	if(userlist_empty(list))
		return "1104"; // Login incorreto, tente novamente
	else{
		while(current!=NULL){
			if(strcmp(user, current->user)==0){
				if(strcmp(pass, current->password)==0)
					return "1100"; // Utilizador identificado com sucesso
			}
			else
				return "1104"; // Login incorreto, tente novamente
			current=user_next(current);
		}
	return "1104"; // Login incorreto, tente novamente
	}
}


char* user_register(struct userlist* list, char* user, char* pass, int nivel)
{
	struct user_node* current=list->head;
	if(userlist_empty(list)){
		userlist_insert(list, user, pass, nivel);
		return "1101"; // Utilizador registado com sucesso
	}
	else{
		while(current!=NULL){
			if(strcmp(user, current->user)==0){
				return "1103"; // Username já existente no sistema, insira novo username
			}
			else{
				if(current->next == NULL){
					userlist_insert(list, user, pass, nivel);
					return "1101"; // Utilizador registado com sucesso
				}
			}
			current=user_next(current);
		}
	}
	return "1105"; // Registo incorreto
}
