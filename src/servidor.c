#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#ifdef WIN32		// se o SO for windows
#include <winsock2.h>
WSADATA wsa_data;
#else				// se o SO for linux/unix
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include "user_list.h"
#include "contas_list.h"


int main() {
	#ifdef WIN32	// se o SO for windows
	WSAStartup(MAKEWORD(2, 0), &wsa_data);
	#endif

	struct sockaddr_in cliente; // preciso de outro para o cliente

	// Definição e população da estructura sockaddr_in -------------------------
	struct sockaddr_in servidor;
	memset(&(servidor.sin_zero),0, sizeof(servidor.sin_zero));
	servidor.sin_family = AF_INET;
	servidor.sin_addr.s_addr = htonl(INADDR_ANY);
	servidor.sin_port = htons(2000); //servidor na porta 2000

	//criar o socket	--------------------------------------------------------
	int sock = socket(AF_INET,SOCK_STREAM,0);

	//fazer o bind	------------------------------------------------------------
	int bindResult = bind(sock, (struct sockaddr *) &servidor, sizeof(servidor));
	if (bindResult==-1) // verifica se houve erros no bind
		printf("Bind: Falhou!\n");
	else
		printf("Bind: ok!\n");

	// fazer o listen com uma fila de 1 cliente no máximo ----------------------
	int tamanho_fila = 1;
	if (listen(sock,tamanho_fila) == -1)  // verifica se o listen falhou
		printf("Listen: Falhou!\n");
	else
		printf("Listen: ok!\n");


	struct userlist* userlist = userlist_new();
	struct contalist* contalist = contalist_new();

	bool login = false;
	struct user_node* user_node = malloc(sizeof(struct user_node));
	char cod_send[4];

	char command[4], args1[32], args2[32], args3[128];
	char received[2048];	// variável para guardar o que é lido do socket
	char to_send[2048];		// variável para guardar a resposta para o cliente

	int nbytes;				// guarda o número de bytes da mensagem lida
	socklen_t tamanho_cliente;	// guarda o tamanho da estructura do 
	int sock_aceite;		// guarda socket aceite



	tamanho_cliente = sizeof(struct sockaddr_in); //por imposição do accept temos que fazer isto

	while(1){ // o servidor vai estar sempre a correr

		// fica à espera que um cliente se ligue
		sock_aceite = accept(sock, (struct sockaddr *)&cliente, &tamanho_cliente);

		if(sock_aceite>-1) {	//se recebeu um cliente
			//mostra informações do cliente
			printf("Recebida uma ligação do cliente IP: %s\n",inet_ntoa(cliente.sin_addr));

			// lê uma mensagem do socket
			nbytes = recv(sock_aceite, received, sizeof(received)+1, 0);
			if(nbytes>0){
				printf("Mensagem recebida: \"%s\"\n", received);

				if(received[0] == '1'){ // 1XXX login e registo
					if(received[3] == '0'){ // 1000 login
						sscanf(received,"%s %s %s", command, args1, args2);
						strcpy(cod_send, user_login(userlist, args1, args2));
						if(strcmp(cod_send, "1100")==0){
							strcpy(user_node->user, args1);
							login = true;
						}
						sprintf(to_send, "%s", cod_send);
					}
					else if(received[3] == '1'){ // 1001 registo de cliente
							sscanf(received,"%s %s %s", command, args1, args2);
							strcpy(cod_send, user_register(userlist, args1, args2, 1));
							if(strcmp(cod_send, "1101")==0){
								strcpy(user_node->user, args1);
								login = true;
							}
							sprintf(to_send, "%s", cod_send);
						}
					else if(received[3] == '2'){ // 1002 registo de entidade
							sscanf(received,"%s %s %s", command, args1, args2);
							strcpy(cod_send, user_register(userlist, args1, args2, 2));
							if(strcmp(cod_send, "1101")==0){
								strcpy(user_node->user, args1);
								login = true;
							}
							sprintf(to_send, "%s", cod_send);
						}
					else if(received[3] == '3'){ // 1004 registo de administrador
							sscanf(received,"%s %s %s", command, args1, args2);
							strcpy(cod_send, user_register(userlist, args1, args2, 3));
							if(strcmp(cod_send, "1101")==0){
								strcpy(user_node->user, args1);
								login = true;
							}
							sprintf(to_send, "%s", cod_send);
						}
				}
				else {
					if(!login){
						strcpy(cod_send, "5000"); // precisa de login
						sprintf(to_send, "%s", cod_send);
					}
					else{
						if(received[0] == '2'){ // 2XXX contas
							if(received[3] == '0'){ // 2000 - Pede para criar conta
								sscanf(received,"%s %s", command, args1);
								strcpy(cod_send, cria_conta(contalist, userlist, user_node->user));
								sprintf(to_send, "%s", cod_send);
							}
							else
								if(received[3] == '1'){ // 2001 - Pede para consultar o saldo de uma conta
									sscanf(received,"%s %s", command, args1);
									char nib[21];
									strncpy(nib, args1, 21);
									strcpy(cod_send, consulta_saldo(contalist, userlist, user_node->user, nib));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '2'){ // 2002 - Pede para consultar movimento de uma conta
									sscanf(received,"%s %s", command, args1);
									char nib[21];
									strncpy(nib, args1, 21);
									strcpy(cod_send, consulta_movimentos(contalist, userlist, user_node->user, nib));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '3'){ // 2003 - Pede para consultar saldo integrado
									sscanf(received,"%s %s", command, args1);
									strcpy(cod_send, consulta_saldo_integrado(contalist, userlist, user_node->user));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '4'){ // 2004 - Pede para movimentar contas próprias
									sscanf(received,"%s %s %s %s", command, args1, args2, args3);
									char nib1[21];
									strncpy(nib1, args1, 21);
									char nib2[21];
									strncpy(nib2, args2, 21);
									//char v[3];
									//strncpy(v, args3, 3);
									printf("args3: \"%s\"\n", args3);
									//printf("v: \"%s\"\n", v);
									int valor = atoi(args3);
									printf("%d\n", valor);
									strcpy(cod_send, transferencia(contalist, userlist, user_node->user, nib1, nib2, valor));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '5'){ // 2005 - Pede para apagar conta
									sscanf(received,"%s %s", command, args1);
									char nib1[21];
									strncpy(nib1, args1, 21);
									char nib2[21];
									strncpy(nib2, args2, 21);
									strcpy(cod_send, conta_remove(contalist, userlist, user_node->user, nib1, nib2));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '6'){ // 2006 - Pede uma lista de contas próprias
									sscanf(received,"%s %s", command, args1);
									strcpy(cod_send, lista_contas(contalist, userlist, user_node->user));
									sprintf(to_send, "%s", cod_send);
								}
						}
						else 
							if(received[0] == '3'){ // 3XXX serviços
								if(received[3] == '0'){ // 3000 - Listagem de Serviços de Pagamento
									strcpy(cod_send, "5001");
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '1'){ // 3001 - Criação de novo Serviço
									strcpy(cod_send, "5001");
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '2'){ // 3002 - Efetuar pagamento de serviço
									strcpy(cod_send, "5001");
									sprintf(to_send, "%s", cod_send);
								}
							}
						else {
							if(received[0] == '4'){ // 4XXX administração
								if(received[3] == '1'){ // 4001 - Pede Listar todos os Clientes e os respetivos saldos
									strcpy(cod_send, "5001");
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '2'){ // 4002 - Pede Listar todos as Entidades e os respetivos saldos
									strcpy(cod_send, "5001");
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '3'){ // 4003 - Pede Valor total depositado no banco
									strcpy(cod_send, total_depositado(contalist, userlist, user_node->user));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '4'){ // 4004 - Pede Congelar Conta (de Cliente ou de Entidade)
									sscanf(received,"%s %s", command, args1);
									strcpy(cod_send, congela_conta(contalist, userlist, user_node->user, args1));
									sprintf(to_send, "%s", cod_send);
								}
							else
								if(received[3] == '5'){ // 4005 - Pede Descongelar Conta (de Cliente ou de Entidade)
									sscanf(received,"%s %s", command, args1);
									strcpy(cod_send, descongela_conta(contalist, userlist, user_node->user, args1));
									sprintf(to_send, "%s", cod_send);
								}
							}
						}
					}
				}
			}
			printf("Mensagem enviada: \"%s\"\n", to_send);
			send(sock_aceite, to_send, strlen(to_send)+1, 0);
			close(sock_aceite);
		}
	}
}